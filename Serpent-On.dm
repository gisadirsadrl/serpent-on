#modname "Serpent-On 0.11"
#descr "A collection of nation modifications. Expands thematic elements of nations where they are overshadowed by incident. Includes: Mictlan (EA, MA, LA), Ragha. Also bans magic duel."

-- Targets:
-- Mictlan (DONE)
-- Ragha (DONE)
-- Midgard
-- Utgard
-- Machaka
-- All water nations:
-- EA R'lyeh needs something interesting.
-- MA R'lyeh needs more culture and diversity.
-- LA R'lyeh needs to be not exactly the same.
-- EA Atlantis needs a more defined coastal moiety.
-- MA Atlantis needs to be more distinct. Focus on the Coral and throw away the cultists (or make them a different nation).
-- It's also worth taking a close look at all returning Atlanteans, from two Xibalbas, Mictlan, and LA Atlantis.
-- EA Pelagia should have more Ichytids.
-- Oceania needs to be merged into Pangaea.
-- Heroes with long lifespans need differen incarnations.
-- All water nations need land recruits and some land nations (e.g. Agartha should have more water recruits)
-- MA Agartha needs statues to be more relevant.
-- Machaka

#version 0.11

--ORDER
--Units
--Sites
--Nations
--Events

#selectspell 523 -- Magic Duel
#school -1
#end


#selectweapon 576  --Weak Bite, changed for LA Mictlan
#dmg 1
#dt_normal
#bowstr
#end

--EA Mictlan: make cap-onlies more powerful with random magic, add a less powerful sun priest and make priest king non cap-only. Redo the sacreds (Eagle Warriors go to MA, for example.)
#selectmonster 732 -- Priest King (no longer cap-only)
#custommagic 19072 10
#rpcost 4
#descr "The Priest Kings are the temporal leaders of the kingdom. They bless the Land with fertility and lead the people. The Priest King wields priestly power and is skilled in Nature and Blood magic. He can levy slave warriors. The Priest King represents the relationship between centralized authority and the dominion of the Land."
#end
#selectmonster 733 -- Rain Priest (recruitable in third fort)
#custommagic 26752 100
#custommagic 10880 10
#rpcost 4
#descr "Rain is important in the Mictlan Cult. Unless sated with human sacrifices, the God will withhold the life-giving Rain from the Land. The Rain Priest wields priestly power and is skilled in Water and Blood magic."
#end
#selectmonster 734 -- Moon Priest (recruitable in second fort)
#custommagic 25216 100
#custommagic 10880 10
#rpcost 4
#descr "The Moon is the Dream Face of God in the Mictlan Cult. Unless sated with human sacrifices, the Moon will not bless the dreams of his followers. The Moon Priest wields priestly power and is skilled in Astral and Blood magic."
#end
#selectmonster 735 -- High Priest of the Sun
#custommagic 10880 100
#custommagic 27264 10
#rpcost 4
#descr "The Sun is the Watchful Face of God in the Mictlan Cult. Unless sated with human sacrifices, it will look to other, more worthy followers. The High Priest of the Sun is the most important of all priests of the Empire. His authority outweighs even the authority of the Priest Kings. The High Priest is skilled in Fire magic and is a powerful priest and Blood mage."
#end

#newmonster 8801
#copystats 1361
#copyspr 1361
#researchbonus -4
#shapechange 8802
#end
#newmonster 8802
#copystats 1362
#copyspr 1362
#researchbonus -4
#shapechange 8801
#end

#selectmonster 725 -- Sun Warrior
#sunawe 1
#gcost 25
#descr "There are two faces of God in the Mictlan worship: The Dream Face of the Moon and the Watchful and Angry Face of the Sun. There are two orders of elite warriors corresponding to the two Faces. Sun Warriors are elite warriors who fight in the manner of the Sun, armed with small hatchets of bronze, a material of the day. They are blessed by the Sun and are considered sacred. Their blessed status has granted them the shining aura of the sun when fighting under it."
#end

#newmonster 8800 -- Rain Slave (recruitable in third fort)
#copystats 1882
#name "Rain Communicant"
#spr1 "./Serpent-On/rain_communicant.tga"
#spr2 "./Serpent-On/rain_communicant.tga"
#holy
#mr 10
#darkvision 0
#reinvigoration 1
#gcost 50
#comslave
#reclimit 1
#clearweapons
#descr "The Rain is a giver of life and sustenance. At the Temple of the Rain, slaves are given sacred herbs that send them into a deep trance. From there, their life force is available to aid the magic of nearby priests, even though they are not of pure blood."
#end

#selectmonster 1882 -- Moon Warrior (recruitable in second fort)
#spr1 "./Serpent-On/moon_warrior.tga"
#spr2 "./Serpent-On/moon_warrior_att.tga"
#holy
#mr 13
#descr "There are two faces of God in the Mictlan worship: The Dream Face of the Moon and the Watchful and Angry Face of the Sun. There are two orders of elite warriors corresponding to the two Faces. Moon Warriors are elite warriors who fight in the manner of the Moon, armed with swords of obsidian, a material of the night. They are blessed by the Moon and are considered sacred. Their blessed status has granted them night vision and increased resistance to magic."
#end

#selectsite 33 -- High Temple of the Sun
#clear
#rarity 5
#path 7
#gems 0 3
#gems 7 3
#homecom 735
#homemon 725
#end

#selectsite 32 -- EA Temple of the Moon
#clear
#rarity 5
#path 7
#gems 4 2
#gems 7 2
#nat 12
#natcom 734
#natmon 1882
#end

#selectsite 31 -- EA Temple of the Rain
#clear
#rarity 5
#path 7
#gems 2 2
#gems 7 2
#nat 12
#natcom 733
#natmon 8800
#end

#selectnation 12
#descr "Mictlan is an ancient tribal empire that has been isolated for centuries. The foul practices of the priest-kings of Mictlan have caused most of their neighbors to leave or else face slavery and blood sacrifice. Since the dawn of the kingdom, blood has been spilled in the temples of the capital to feed the waning powers of the Hungry God. The armies are mainly composed of slaves from newly conquered lands. Three orders of mage-priests in three cities celebrate three aspects of the Hungry God under the supervision of the priest-kings. The Watchful Face of God, the Sun, is exalted in the capital. In the second city, the Dream Face of God is revered by the Moon Priests. The Rain Priests and their communicants rule the third city. All are skilled blood mages."
#startscout 732
#clearrec
#addrecunit 721
#addrecunit 722
#addrecunit 723
#addrecunit 724
#addrecunit 860
#addreccom 729
#addreccom 730
#addreccom 731
#addreccom 732
#addforeigncom 8801
#defcom2 732
#defunit2 1546
#defunit2b 1547
#clearsites
#startsite "High Temple of the Sun"
#end

#newevent
#rarity 5
#req_unique 1
#req_fornation 12
#req_capital 0
#req_fort 1
#req_land 1
#msg "The Temple of the Moon has been renovated in ##landname##. Faith has increased."
#addsite 32
#incdom 3
#end

#newevent
#rarity 5
#req_unique 1
#req_fornation 12
#req_capital 0
#req_fort 1
#req_land 1
#req_nositenbr 32
#msg "The Temple of the Rain has been renovated in ##landname##. Faith has increased."
#addsite 31
#incdom 3
#end

--MA Mictlan

#selectmonster 1888 -- Sky Priest
#clearmagic
#magicskill 1 1
#magicskill 8 1
#custommagic 10880 100
#descr "With the coming of the Lawgiver, the religious structure of Mictlan was altered. Blood sacrifices have been abolished and the Sky, abode of the Lawgiver, has received a new and important role. From the skies He observes the actions of the Mictlan people and judges them accordingly. In the skies His two faces, the Sun and the Moon, smile upon the righteous and give them strength of heart and prophetic dreams. From the skies falls the life-giving Rain and from the skies comes harsh judgment in the form of thunder. The Sky Priests serve as judges of the provinces. They travel the land and mete out justice and guide the locals. They are trained in Air magic, but a few of them have studied the other manifestations of the God. Sky Priests have a bonus when patrolling provinces."
#end

#selectmonster 1190 -- Priest King (no longer cap-only)
#clearmagic
#magicskill 6 2
#magicskill 8 2
#custommagic 2944 10
#rpcost 4
#descr "Priest Kings are the temporal leaders of the kingdom. They bless the Land with fertility and lead the people. They lead the people in accordance with the laws of the Sky Priests. The Priest King wields priestly power and is skilled in Nature magic. He can levy slave warriors. The Priest King represents the relationship between centralized authority and the dominion of the Land."
#end

#selectmonster 1191 - Rain Priest (first additional fort)
#custommagic 10624 100
#custommagic 43904 10
#magicboost 7 1
#descr "The Rain is important in the Mictlan Cult. With the coming of the Lawgiver, Rain Priests have given up Blood magic. The Rain Priest wields priestly power and is skilled in Water magic."
#end

#selectmonster 1192 - Moon Priest (second additional fort)
#custommagic 9088 100
#custommagic 43904 10
#magicboost 7 1
#descr "The Moon is the Dream Face of God in the Mictlan Cult. With the coming of the Lawgiver, Moon Priests have given up Blood magic. The Moon Priest wields priestly power and is skilled in Astral magic."
#end

#selectmonster 1193 - Sun Priest (third additional fort)
#custommagic 27392 100
#custommagic 60288 10
#magicboost 7 1
#descr "The Sun is the Watchful Face of God in the Mictlan Cult. With the coming of the Lawgiver, the Sun Priest has lost some of the importance he enjoyed in former times. He wields priestly power and is skilled in Fire magic. There are rumors that some Sun Priests secretly practice the blood magic of old, their foul rituals still supposedly keeping the sun shining."
#end

#selectmonster 1361 -- Nahualli (foreignrec)
#descr "The Nahualli are Mictlan sorcerers. They have little influence in society and live secluded lives in rural areas. They are beast-mages and shamans, able to bind animals to their service by entering the spirit world. Nahualli are also able to shapechange into their animal spirit double. With the emergence of the Couatl, the Nahualli have been convinced to share their magical knowledge with Mictlan."
#end

#selectsite 87 -- High Temple of the Land
#clear
#name "Jungle of the Couatls"
#rarity 5
#path 6
#gems 4 1
#gems 6 2
#homecom 1194
#homemon 727
#end

#selectsite 88 -- High Temple of the Sky and the Rain
#clear
#name "High Temple of the Sky"
#rarity 5
#path 1
#gems 1 2
#homecom 1907
#end

#selectsite 89 -- MA Temple of the Moon
#clear
#name "Old Temple of the Moon"
#rarity 5
#path 4
#gems 4 1
#nat 51
#natcom 1192
#end

#selectsite 90 -- MA Temple of the Sun
#clear
#name "Old Temple of the Sun"
#rarity 5
#path 0
#gems 0 1
#nat 51
#natcom 1193
#end

#selectsite 30 -- EA Temple of the Land (being used)
#clear
#name "Old Temple of the Rain"
#rarity 5
#path 2
#gems 2 1
#nat 51
#natcom 1191
#end

#newspell
#name "Law Master"
#descr "The mage who has cast this spell can use the magic power of the mages who have cast Law Slave. The fatigue that comes from casting spells will be distributed among all communion members and the communion master will also be able to cast more powerful spells than he could alone. While in communion, all spells that only affect the caster will also affect all the communion slaves. A communion with two communion slaves will grant all masters one extra level in all their paths, four slaves will grant two levels, eight slaves will grant three levels, and so on."
#school -1
#end


#selectnation 51
#descr "Mictlan is an ancient tribal empire that has been isolated for centuries. The foul practices of the priest-kings of Mictlan have caused most of their neighbors to leave or else face slavery and blood sacrifice. Since the dawn of the kingdom, blood has been spilled in the temples of the capital. Now a new era has dawned and the isolation is broken. The Blood Cult has been abolished and the sky priests now worship the Lawgiver. The armies are mainly composed of slaves from newly conquered lands. Three ancient orders of mage-priests in three cities still celebrate three aspects of the Lawgiver under the supervision of the priest-kings. In the second city, the Dream Face of God is revered by the Moon Priests. The Rain Priests and their communicants rule the third city. The remnants of Sun Priests follow the Watchful Face of God from the fourth city."
#startscout 1190
#clearrec
#addrecunit 721
#addrecunit 1545
#addrecunit 1546
#addrecunit 1547
#addrecunit 1548
#addrecunit 726
#addreccom 729
#addreccom 730
#addreccom 1888
#addreccom 1190
#addforeigncom 1361
#defcom2 1190
#clearsites
#startsite "Jungle of the Couatls"
#startsite "High Temple of the Sky"
#end

#newevent
#rarity 5
#req_unique 1
#req_fornation 51
#req_capital 0
#req_fort 1
#req_land 1
#msg "The Old Temple of the Rain has been renovated in ##landname##. Faith has increased."
#addsite 30
#incdom 1
#end

#newevent
#rarity 5
#req_unique 1
#req_fornation 51
#req_capital 0
#req_fort 1
#req_land 1
#req_nositenbr 30
#msg "The Old Temple of the Moon has been renovated in ##landname##. Faith has increased."
#addsite 89
#incdom 1
#end

#newevent
#rarity 5
#req_unique 1
#req_fornation 51
#req_capital 0
#req_fort 1
#req_land 1
#req_nositenbr 30
#req_site 0
#msg "The Old Temple of the Sun has been renovated in ##landname##. Faith has increased. [Old Temple of the Moon]"
#addsite 90
#incdom 2
#end

--LA Mictlan

#selectmonster 1424 -- Sun Priest
#custommagic 27136 100
#custommagic 10880 10
#rpcost 4
#descr "The Sun is the Watchful face of God in the Mictlan Cult. Unless sated with human sacrifices, it will look to other, more worthy followers. The Sun Priest is not as important as he was in earlier times. He wields priestly power and is skilled in Fire and Blood magic."
#end

#selectmonster 2894
#copystats 207
#name "Shambler General"
#descr "...."
#spr1 "./Serpent-On/shambler_general.tga"
#spr2 "./Serpent-On/shambler_general_att.tga"
#rpcost 2
#female
#berserk 3
#okleader
#hp 30
#str 17
#att 13
#def 12
#prec 8
#prot 6
#size 3
#mr 13
#mor 13
#enc 3
#mapmove 16
#ap 11
#holy
#clearweapons
#weapon 671
#weapon 671
#weapon 576
#ambidextrous 2
#cleararmor
#armor 152
#armor 188
#clearmagic
#magicskill 2 1
#magicskill 7 1
#magicskill 8 2
#custommagic 29184 100
#end

#selectmonster 2895
#copystats 207
#name "Mother of Sharks"
#descr "The Rain Cult has little traction in the world under the waves. Instead, a complementary tradition has arisen, imitating the shark. Sharks care nothing for the bonds of kinship and sentimentality, so their sacred bodies are processed into armaments. The Mothers of Sharks are warrior priestesses decked in sharkskin who are driven to a fury by the sight of blood."
#spr1 "./Serpent-On/mother_of_sharks.tga"
#spr2 "./Serpent-On/mother_of_sharks_att.tga"
#rpcost 2
#female
#berserk 3
#okleader
#hp 30
#str 17
#att 13
#def 12
#prec 8
#prot 6
#size 3
#mr 13
#mor 13
#enc 3
#mapmove 16
#ap 11
#holy
#clearweapons
#weapon 671
#weapon 671
#weapon 576
#ambidextrous 2
#cleararmor
#armor 152
#armor 188
#clearmagic
#magicskill 2 1
#magicskill 7 1
#magicskill 8 2
#custommagic 29184 100
#end

#selectmonster 2896
#copystats 207
#name "Shark Warrior"
#descr "The Rain Cult has little traction in the world under the waves. Instead, a complementary tradition has arisen, imitating the shark. Sharks care nothing for the bonds of kinship and sentimentality, so their sacred bodies are processed into armaments. Sacred warriors are driven to a fury by the sight of blood."
#spr1 "./Serpent-On/shark_warrior.tga"
#spr2 "./Serpent-On/shark_warrior_att.tga"
#female
#berserk 2
#okleader
#hp 16
#str 13
#att 11
#def 10
#prec 8
#prot 2
#size 2
#mr 11
#mor 13
#enc 2
#mapmove 12
#ap 10
#holy
#clearweapons
#weapon 671
#weapon 671
#weapon 576
#ambidextrous 2
#cleararmor
#armor 152
#armor 188
#end

#selectsite 103 -- LA Temple of the Rain
#clear
#name "High Temple of the Rain"
#rarity 5
#path 7
#gems 2 3
#gems 7 3
#homecom 1420
#homemon 1423
#end

#selectsite 104 -- LA Temple of the Moon
#clear
#name "Temple of the Moon"
#rarity 5
#path 7
#gems 4 1
#gems 7 2
#nat 86
#natcom 734
#end

#selectsite 105 -- LA Temple of the Sun
#clear
#name "Temple of the Sun"
#rarity 5
#path 7
#gems 0 1
#gems 7 2
#nat 86
#natcom 1424
#end

#selectsite 102 -- LA Temple of the Land
#clear
#name "Temple of Sharks"
#rarity 5
#path 2
#gems 2 1
#nat 86
#natmon 2896
#natcom 2895
#end

#selectnation 86
#descr "Mictlan is an ancient tribal empire that has been isolated for centuries. The foul practices of the priest-kings of Mictlan have caused most of their neighbors to leave or else face slavery and blood sacrifice. Since the dawn of the kingdom, blood has been spilled in the temples of the capital, until the practice was disappeared. When the Kings of Rain arrived, fleeing the ruins of Atlantis, they discovered the secrets of the long abandoned Blood Cult and reinstated the foul practices of ancient times. Then the New God rose, hungry for blood. Now Mictlan is once again expanding its territories to feed the hunger of its Lord. Isolation has made Mictlan a backward nation and its warriors use archaic weaponry and armor. The armies are mainly composed of slaves from newly conquered lands. Ancient orders of mage-priests in two cities still celebrate the Sun and the Moon under the supervision of the priest-kings. Underwater, the Kings of Rain have set up a proxy cult that worships sharks."
#startscout 732
#clearrec
#addrecunit 721
#addrecunit 1545
#addrecunit 1546
#addrecunit 1547
#addrecunit 1548
#addreccom 729
#addreccom 730
#addreccom 731
#addreccom 732
#addreccom 1421
#addforeigncom 8801
#uwrec 110
#uwrec 107
#uwrec 111
#uwrec 1621
#uwrec 206
#uwrec 2862
#uwcom 432
#uwcom 207
#defcom2 732
#defunit2 1546
#defunit2b 1547
#clearsites
#startsite "High Temple of the Rain"
#end

#newevent
#rarity 5
#req_unique 1
#req_fornation 86
#req_capital 0
#req_fort 1
#req_land 1
#msg "The Temple of the Moon has been renovated in ##landname##. Faith has increased."
#addsite 104
#incdom 1
#end

#newevent
#rarity 5
#req_unique 1
#req_fornation 86
#req_capital 0
#req_fort 1
#req_land 1
#req_nositenbr 104
#msg "The Temple of the Sun has been renovated in ##landname##. Faith has increased."
#addsite 105
#incdom 1
#end

#newevent
#rarity 5
#req_unique 1
#req_fornation 86
#req_capital 0
#req_fort 1
#req_land 0
#msg "The Temple of the Sharks has been constructed in ##landname##. Faith has increased."
#addsite 102
#incdom 1
#end


--EA Ragha: Shift to seasonal instead of temperature recruits.

#newmonster 8900
#copystats 2587
#copyspr 2587
#heatrec -3
#summershape 2587
#landdamage 100
#descr "The Zhayedan, Immortals, is a Turan sacred elite force of limited numbers, replaced only when one of their numbers is slain. Once they were an infantry unit, but with the increasing interest in mounted warfare and the acquisition of gryphons from a tribe of Amazons, the Immortals became an airborne cavalry unit of unequaled power. If the Zhayedan is killed, his gryphon will fight on, and if the gryphon is returned to Ragha another Zhayedan will mount the riderless Gryphon.

Zhayedan are only recruitable during the summer. Recruiting them during any other season will fail."
#end
#newmonster 8901
#copystats 2592
#copyspr 2592
#coldrec -3
#wintershape 2592
#landdamage 100
#descr "When the Airyas fled from Caelum, they found refuge in the desolate plain of Ragha, where they had once fought the Turan people. The last of the Airya Iceclads protected the last of the Seraphs during the negotiations with the Turan Shah and they became guardians of the emerging priestly caste. The Iceclads soon adopted the Turan concept of the sacred Immortals. The Immortals, Zhayedan, is a sacred elite force of limited numbers, replaced only when one of their numbers is slain. While the Turan Immortals bring righteous wrath to the enemies, the Airya Iceclads became serene guardians of the sacred flames and upholders of the faith.

Icelad Zhayedan are only recruitable during the winter. Recruiting them during any other season will fail."
#end
#newmonster 8902
#copystats 2599
#copyspr 2599
#heatrec -3
#springshape 2599
#summershape 2599
#autumnshape 2599
#landdamage 100
#descr "Turan society has been stratified since the arrival of the Abysians. A few families of pure blood have made themselves priest-mages and jealously guard their privileges. The priest-mages of these families are known as Magi. The Karapan is a Turan Magus of the old Daevic cult. They worship the Daevas and have used tainted magic since before the forming of Ragha. They still use Blood and Death magic in conjunction with Fire magic. To the Karapan, the flames must be fed the flesh and blood of the living in order to gain power.

Karapan are not recruitable during the winter. Recruiting them during winter will fail."
#end
#newmonster 8903
#copystats 2600
#copyspr 2600
#heatrec -3
#summershape 2600
#landdamage 100
#descr "Turan society has been stratified since the arrival of the Abysians. A few families of pure blood have made themselves priest-mages and jealously guard their privileges. The priest-mages of these families are known as Magi. The Dastur is a Turan High Magus of the old Daevic cult. They worship the Daevas and have used tainted magic since before the forming of Ragha. They still use Blood and Death magic in conjunction with Fire magic. To the Dastur, the flames must be fed the flesh and blood of the living in order to gain power. The Zaotar and the Dastur share power and influence in the courts of the Shahs. The two priestly orders despise each other and bickering and plotting for power is common in the courts. For a Shah to be inaugurated, a High Magus must be present and the two orders compete for the right to perform the rituals of inaugurating a Shah to office.

Dastur are only recruitable during the summer. Recruiting them during any other season will fail."
#end
#newmonster 8904
#copystats 2601
#copyspr 2601
#heatrec -3
#summershape 2601
#landdamage 100
#descr "While most Turans are of Humanbred stock, there are a few remaining pure-blooded Abysians in the kingdom. These families have preserved the old Abysian magic and jealously guard their secrets. They form a sorcerer-caste of powerful mages, influential in the courts of the Turan Shahs. With the arrival of the Airyas, some of them have turned their attentions towards magic not previously used by the Turans.

Sorcerers are only recruitable during the summer. Recruiting them during any other season will fail."
#end
#newmonster 8905
#copystats 2602
#copyspr 2602
#coldrec -3
#autumnshape 2602
#wintershape 2602
#springshape 2602
#landdamage 100
#descr "When the Airyas arrived Tur was a stratified society. Families of hereditary priest-mages known as Magi had much influence in the kingdom. The Airya Seraphs adopted the Turan traditions and made themselves Magi of the Airyas. Now there are Magi families of both peoples and the stratification permeates the kingdom. The Athravan, Flame Keeper, is an Airya magus tending the sacred flames of the temples. Since fire is sacred to both peoples of Ragha, they both have Magi tending the sacred fires. The Athravan of Airya follow a tradition where flames must be kept pure from pollution and death. They despise and avoid the Turan Karapans, but recognize their status as priests of the Reawakening God.

Athravan are not recruitable during the summer. Recruiting them during summer will fail."
#end
#newmonster 8906
#copystats 2603
#copyspr 2603
#coldrec -3
#wintershape 2603
#landdamage 100
#descr "When the Airyas arrived Tur was a stratified society. Families of hereditary priest-mages known as Magi had much influence in the kingdom. The Airya Seraphs adopted the Turan traditions and made themselves Magi of the Airyas. Now there are Magi families of both peoples and the stratification permeates the kingdom. The Zaotar is an Airya High Magus skilled in the traditional magic of the Airya. The Zaotar and the Dastur share power and influence in the courts of the Shahs. The two priestly orders despise each other and bickering and plotting for power is common in the courts. For a Shah to be inaugurated, a High Magus must be present and the two orders compete for the right to perform the rituals of inaugurating a Shah to office.

Zaotar are only recruitable during the winter. Recruiting them during any other season will fail."
#end
#newmonster 8907
#copystats 2604
#copyspr 2604
#coldrec -3
#wintershape 2604
#landdamage 100
#descr "While most Airyas skilled in magic are trained as Magi, the priest-mages of Ragha, there are a few families who keep the old forms of magic alive. They form a caste of powerful mages, influential in the courts of the Airya Shahs. The Seraphs are powerful mages of wind and ice, but some have learned the Turan ways of Death and Blood as well.

Seraphs are only recruitable during the winter. Recruiting them during any other season will fail."
#end
#newmonster 8908
#copystats 2605
#copyspr 2605
#heatrec -3
#summershape 2605
#landdamage 100
#descr "The Shahs are petty kings of Ragha. There are Turan as well as Airya Shahs, but they are likewise limited in power. The power of the Shah stems from the kingdom and not from the Shah himself. Unless inaugurated by a High Magus, his powers are insignificant in the eyes of the Raghans. One among the Shahs can be appointed Shahanshah, King of Kings. The power of the Shahanshah is linked to the land and the land is linked to the Crown forged by the orders of the High Magi. Only the Shah given the Crown of the Shah can wield the full sacred powers of the kingdom. At times the Shahanshah has been Airya, at times Turan. The Shahs are given priestly authority upon inauguration and are considered sacred, regardless of heritage.

Turan Shah are only recruitable during the summer. Recruiting them during any other season will fail."
#end
#newmonster 8909
#copystats 2606
#copyspr 2606
#coldrec -3
#wintershape 2606
#landdamage 100
#descr "The Shahs are petty kings of Ragha. There are Turan as well as Airya Shahs, but they are likewise limited in power. The power of the Shah stems from the kingdom and not from the Shah himself. Unless inaugurated by a High Magus, his powers are insignificant in the eyes of the Raghans. One among the Shahs can be appointed Shahanshah, King of Kings. The power of the Shahanshah is linked to the land and the land is linked to the Crown forged by the orders of the High Magi. Only the Shah given the Crown of the Shah can wield the full sacred powers of the kingdom. At times the Shahanshah has been Airya, at times Turan. The Shahs are given priestly authority upon inauguration and are considered sacred, regardless of heritage.

Airya Shah are only recruitable during the winter. Recruiting them during any other season will fail."
#end
#newmonster 8910
#copystats 2629
#copyspr 2629
#heatrec -3
#summershape 2629
#landdamage 100
#descr "The Zhayedan Spahbed is a commander of the Zhayedan. They are skilled generals and mighty warriors blessed by the magi priests of the kingdom.

Zhayadan Spahbed are only recruitable during the winter. Recruiting them during any other season will fail."
#end

#selectsite "Lake Urmia"
#clear
#rarity 5
#path 2
#gems 1 1
#gems 2 1
#gems 4 1
#homemon 8901
#end

#selectsite "Mountain of the Everburning Caverns"
#clear
#rarity 5
#path 0
#gems 0 1
#homecom 8910
#homemon 8900
#end

#selectnation 100
#clearrec
#addrecunit 2579
#addrecunit 2580
#addrecunit 2581
#addrecunit 2582
#addrecunit 2583
#addrecunit 2584
#addrecunit 2585
#addrecunit 2586
#addrecunit 2589
#addrecunit 2590
#addrecunit 2591
#addreccom 2588
#addreccom 2595
#addreccom 2596
#addreccom 2597
#addreccom 8902
#addreccom 8903
#addreccom 8904
#addreccom 8905
#addreccom 8906
#addreccom 8907
#addreccom 8908
#addreccom 8909
#end
